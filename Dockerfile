FROM node:8.16-alpine

RUN mkdir -p /api

WORKDIR /api

COPY package*.json /api/

RUN npm install && npm cache clean --force

COPY . /api

RUN npm run build

CMD ["npm", "run", "start:prod"]
