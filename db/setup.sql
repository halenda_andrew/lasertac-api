CREATE SCHEMA IF NOT EXISTS `lasertac` DEFAULT CHARACTER SET utf8 ;
USE `lasertac` ;



-- -----------------------------------------------------
-- Table `lasertac`.`statistic`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lasertac`.`statistic` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `steps` INT NOT NULL DEFAULT 0,
  `total_battles` INT DEFAULT 0,
  `total_ammo` INT DEFAULT 0,
  `total_kills` INT DEFAULT 0,
  `life_lost` INT DEFAULT 0,
  `score_points` INT DEFAULT 0,
  `time_spent` INT DEFAULT 0,
  `likes` INT DEFAULT 0,
  `kda` INT DEFAULT 0,

  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
PACK_KEYS = Default;

-- -----------------------------------------------------
-- Table `lasertac`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `lasertac`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(32) NOT NULL,
  `last_name` VARCHAR(32) NOT NULL,
  `nickname` VARCHAR(32) NOT NULL UNIQUE,
  `gender` VARCHAR(32) NOT NULL,
  `statisticId` INT UNIQUE FOREIGN KEY REFERENCES `statistic(id)`,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 1
PACK_KEYS = Default;