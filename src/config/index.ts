import * as convict from 'convict';

export interface IServerConfig {
  port: number;
  host: string;
}

export interface IAppConfig {
  defaultImgUrl: string;
}

export interface IDbConfig {
  type: string;
  port: number;
  host: string;
  username: string;
  password: string;
  name: string;
}

export interface IConfig {
  environment: string;
  server: IServerConfig;
  app: IAppConfig;
  db: IDbConfig;
}

export const configSchema: convict.Schema<IConfig> = {
  environment: {
    doc: 'The applicaton environment.',
    format: ['production', 'development', 'test'],
    default: 'development',
    env: 'NODE_ENV',
  },
  server: {
    host: {
      doc: 'Server host',
      format: 'ipaddress',
      default: '127.0.0.1',
      env: 'HOST',
    },
    port: {
      doc: 'Server port',
      format: Number,
      default: 3000,
      env: 'PORT',
    },
  },
  app: {
    defaultImgUrl: {
      doc: 'Default image url',
      format: String,
      default: 'https://randomuser.me/api/portraits/men/1.jpg',
    },
  },
  db: {
    type: {
      doc: 'Db Type',
      format: ['mysql', 'postgres'],
      default: 'mysql',
      env: 'DB_TYPE',
    },
    port: {
      doc: 'Db port',
      format: Number,
      default: 3306,
      env: 'DB_PORT',
    },
    host: {
      doc: 'Db host',
      format: '*',
      default: '',
      env: 'DB_HOST',
    },
    username: {
      doc: 'Db username',
      format: String,
      default: 'root',
      env: 'DB_USERNAME',
    },
    password: {
      doc: 'Db user password',
      format: '*',
      default: '',
      env: 'DB_USER_PASSWORD',
    },
    name: {
      doc: 'Db name',
      format: String,
      default: 'test1',
      env: 'DB_NAME',
    },
  },
};

export const config = convict(configSchema).validate();
