import { Injectable } from '@nestjs/common';

import convict = require('convict');
import { IConfig } from '../config';

@Injectable()
export class ConfigService {
  readonly get = this.config.get;

  constructor(private readonly config: convict.Config<IConfig>) {}
}
