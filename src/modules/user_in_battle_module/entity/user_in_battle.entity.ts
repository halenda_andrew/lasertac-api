import { Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn, OneToOne } from "typeorm";
import { BattleTeamsTable } from "src/modules/teams_module/battle_teams.entity";
import { User } from "src/modules/user/user.entity";

@Entity()
export class UserInBattle {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    kills: number;

    @Column()
    death: number;

    @Column()
    assists: number;

    @Column()
    death_time: number;

    @Column()
    plate1_coordinate: string;

    @Column()
    plate2_coordinate: string;

    @Column()
    plate3_coordinate: string;

    @Column()
    used_ammo_count: number;

    @Column()
    steps_count: number;

    @ManyToOne(type => User, user => user)
    user: User;

    @OneToOne(type => BattleTeamsTable)
    @JoinColumn()
    team: BattleTeamsTable;
}