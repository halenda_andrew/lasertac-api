export interface UserInBattleInterface {
    id?: number;
    user_id: number;
    team_id: number;
    kills?: number;
    death?: number;
    assists?: number;
    death_time?: number;
    plate1_coordinate?: string;
    plate2_coordinate?: string;
    plate3_coordinate?: string;
    used_ammo_count?: number;
    steps_count?: number;
}