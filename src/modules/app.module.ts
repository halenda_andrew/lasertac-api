import { Module } from '@nestjs/common';
import { UserModule } from './user/user.module';
import { DatabaseModule } from './database.module';
import { HistoryModule } from './history/history.module';
import { StatisticModule } from './statistic/statistic.module';
import { EquipmentModule } from './equipment/equipment.module';
import { BattleController } from './battle_module/battle.controller';
import { UserGateway } from './user/user.gateway';

@Module({
  imports: [
    DatabaseModule,
    UserModule,
    HistoryModule,
    StatisticModule,
    EquipmentModule,
  ],
  controllers: [
    
  ],
  providers: [],
})
export class AppModule {}
