export interface BattleTeamInterface {
    id?: number;
    team_name: string;
    battle_id: number;
    user_id: number;
}