import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from "typeorm";
import { BattleTable } from "../battle_module/batlle.entity";
import { User } from "src/modules/user/user.entity";

@Entity()
export class BattleTeamsTable {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    team_name: string;

    @ManyToOne(type => BattleTable, BattleTable => BattleTable.team1 || BattleTable.team2)
    battle: BattleTable;

    @ManyToOne(type => User, user => user.teams)
    user: User;

}