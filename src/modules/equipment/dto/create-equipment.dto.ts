import { User } from '../../user/user.entity';

export class CreateEquipmentDto {
  readonly name: string;
  readonly gun_id: string;
  readonly user: User;
}
