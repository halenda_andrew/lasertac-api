import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Equipment } from './equipment.entity';
import { Repository, FindOneOptions } from 'typeorm';
import { CreateEquipmentDto } from './dto/create-equipment.dto';

@Injectable()
export class EquipmentService {
  constructor(
    @InjectRepository(Equipment)
    private readonly equipmentRepository: Repository<Equipment>,
  ) {}

  async addOne(payload: CreateEquipmentDto[]) {
    return this.equipmentRepository.save(payload);
  }

  async findAll(params: FindOneOptions<Equipment>) {
    return this.equipmentRepository.find(params);
  }
}
