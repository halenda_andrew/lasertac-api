import { Entity, PrimaryGeneratedColumn, Column, ManyToOne } from 'typeorm';
import { RepositoryType } from '../../consts';
import { User } from '../user/user.entity';

@Entity(RepositoryType.EQUIPMENT)
export class Equipment {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 32 })
  name: string;

  @Column({ type: 'varchar', length: 32 })
  gun_id: string;

  @ManyToOne(type => User, user => user.equipment, { onDelete: 'CASCADE' })
  user: User;
}
