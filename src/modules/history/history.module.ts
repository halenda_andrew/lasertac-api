import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { History } from './history.entity';
import { HistoryGateway } from './history.gateway';
import { HistoryService } from './history.service';
import { ConfigService } from '../config.service';

@Module({
  imports: [TypeOrmModule.forFeature([History])],
  providers: [HistoryService, HistoryGateway],
})
export class HistoryModule {}
