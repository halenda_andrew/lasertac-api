import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  ManyToMany,
} from 'typeorm';
import { User } from '../user/user.entity';
import { RepositoryType } from '../../consts';

@Entity(RepositoryType.HISTORY)
export class History {
  @PrimaryGeneratedColumn()
  id: number;

  @OneToOne(() => User)
  @JoinColumn()
  admin: User;

  @Column({ type: 'varchar', length: 32 })
  battle_name: string;

  @Column({ type: 'bool' })
  is_public: boolean;

  @Column({ type: 'int' })
  end_time: number;

  @Column({ type: 'float' })
  latitude: number;

  @Column({ type: 'float' })
  longitude: number;

  // @ManyToMany({
  // players
  // })
}
