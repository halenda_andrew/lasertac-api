import { Controller } from '@nestjs/common';
import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Client } from 'socket.io';

import { HistoryService } from './history.service';
import { ErrorType } from '../../consts';

@WebSocketGateway()
export class HistoryGateway {
  @WebSocketServer()
  server: Server;

  constructor(private readonly historyService: HistoryService) {}

  @SubscribeMessage('history')
  async getHistory(_client: Client, payload: any) {
    try {
      const response = await this.historyService.addOne(payload);

      return {
        event: 'userCreated',
        data: response.generatedMaps,
      };
    } catch (err) {
      console.log(err);
      return {
        event: 'serverError',
        data: ErrorType.сreateUserError,
      };
    }
  }
}
