import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, FindOneOptions } from 'typeorm';

import { User } from './user.entity';
import { IUpdateUserBaseInfo, ICreateUserInfo } from './interfaces';
import { RepositoryType } from '../../consts';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async addOne(payload: ICreateUserInfo) {
    return this.userRepository.save(payload);
  }

  async findOne(params: FindOneOptions<User>) {
    return this.userRepository.findOne(params);
  }

  async findOneDetailed(id: number) {
    return this.userRepository.findOne(id, {
      relations: [RepositoryType.STATISTIC, RepositoryType.EQUIPMENT],
    });
  }

  async findAll(ids: number[]) {
    return this.userRepository.findByIds(ids, {
      relations: [RepositoryType.STATISTIC, RepositoryType.EQUIPMENT],
    });
  }

  async updateOne({ id, ...rest }: IUpdateUserBaseInfo) {
    return this.userRepository.update({ id }, rest);
  }
}
