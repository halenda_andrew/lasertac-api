import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Client } from 'socket.io';

import { UserService } from './user.service';
import { CreateUserDto } from './dto/create-user.dto';
import { ErrorType } from '../../consts';
import { IUpdateUserBaseInfo, ICreateUserInfo } from './interfaces';
import { StatisticService } from '../statistic/statistic.service';
import { EquipmentService } from '../equipment/equipment.service';
import { In } from 'typeorm';

@WebSocketGateway()
export class UserGateway {
  @WebSocketServer()
  server: Server;

  constructor(
    private readonly userService: UserService,
    private readonly statisticService: StatisticService,
    private readonly equipmentService: EquipmentService,
  ) {}

  @SubscribeMessage('createUser')
  async createUser(_client: Client, payload: CreateUserDto) {
    try {
      const usData = await this.userService.findOne({
        where: { nickname: payload.nickname },
      });

      if (usData) {
        throw new Error(ErrorType.userAlreadyExists);
      }

      const gunIds = payload.equipment.map(({ gun_id }) => gun_id);

      const eqData = await this.equipmentService.findAll({
        where: { gun_id: In(gunIds) },
      });

      if (eqData.length > 0) {
        throw new Error(ErrorType.equipmentIdExists);
      }
      const statistic = await this.statisticService.addOne();

      const equipment = await this.equipmentService.addOne(payload.equipment);

      const user = await this.userService.addOne({
        ...payload,
        statistic,
        equipment,
      } as ICreateUserInfo);

      return {
        event: 'userCreated',
        data: user,
      };
    } catch (err) {
      console.log(err);

      if (err && err.message && err.message === ErrorType.userAlreadyExists) {
        return {
          event: 'serverError',
          data: ErrorType.userAlreadyExists,
        };
      }

      if (err && err.message && err.message === ErrorType.equipmentIdExists) {
        return {
          event: 'serverError',
          data: ErrorType.equipmentIdExists,
        };
      }

      return {
        event: 'serverError',
        data: ErrorType.сreateUserError,
      };
    }
  }

  @SubscribeMessage('findUser')
  async findUser(_client: Client, id: number) {
    try {
      const response = await this.userService.findOneDetailed(id);

      return {
        event: 'userFound',
        data: response,
      };
    } catch (err) {
      console.log(err);
      return {
        event: 'serverError',
        data: ErrorType.searchUserError,
      };
    }
  }

  @SubscribeMessage('findUsers')
  async findUsers(_client: Client, payload: number[]) {
    try {
      const response = await this.userService.findAll(payload);

      return {
        event: 'usersFound',
        data: response,
      };
    } catch (err) {
      console.log(err);
      return {
        event: 'serverError',
        data: ErrorType.searchUsersError,
      };
    }
  }

  @SubscribeMessage('updateUser')
  async updateUser(_client: Client, payload: IUpdateUserBaseInfo) {
    try {
      await this.userService.updateOne(payload);

      return {
        event: 'userUpdated',
        data: '',
      };
    } catch (err) {
      console.log(err);
      return {
        event: 'serverError',
        data: ErrorType.updateUserError,
      };
    }
  }
}
