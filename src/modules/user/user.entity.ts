import {
  Column,
  Entity,
  PrimaryGeneratedColumn,
  OneToOne,
  JoinColumn,
  OneToMany,
} from 'typeorm';
import { RepositoryType } from '../../consts';
import { config } from '../../config';
import { GenderType } from './interfaces';
import { Statistic } from '../statistic/statistic.entity';
import { Equipment } from '../equipment/equipment.entity';
import { BattleTeamsTable } from '../teams_module/battle_teams.entity';
import { BattleTable } from '../battle_module/batlle.entity';

@Entity(RepositoryType.USER)
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 32 })
  first_name: string;

  @Column({ type: 'varchar', length: 32 })
  last_name: string;

  @Column({ type: 'varchar', length: 32, unique: true })
  nickname: string;

  @Column({ type: 'varchar', length: 32 })
  gender: GenderType;

  @Column({
    type: 'varchar',
    length: 256,
    default: config.get('app.defaultImgUrl'),
  })
  profile_img_url: string;

  @OneToOne(() => Statistic, { onDelete: 'CASCADE' })
  @JoinColumn()
  statistic: Statistic;

  @OneToMany(type => Equipment, equipment => equipment.user, {
    onDelete: 'CASCADE',
  })
  equipment: Equipment[];

  @OneToMany(type => BattleTeamsTable, BattleTeamsTable => BattleTeamsTable.battle)
  teams: BattleTeamsTable[];

  @OneToMany(type => BattleTable, BattleTable => BattleTable.battle_creator)
  created_battles: BattleTable[];

}
