import { GenderType } from '../interfaces';
import { Equipment } from '../../equipment/equipment.entity';

export class CreateUserDto {
  readonly first_name: string;
  readonly last_name: string;
  readonly nickname: string;
  readonly gender: GenderType;

  readonly equipment: Equipment[];
}
