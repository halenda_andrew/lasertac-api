import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { UserGateway } from './user.gateway';
import { User } from './user.entity';
import { UserService } from './user.service';
import { Statistic } from '../statistic/statistic.entity';
import { StatisticService } from '../statistic/statistic.service';
import { EquipmentService } from '../equipment/equipment.service';
import { Equipment } from '../equipment/equipment.entity';

@Module({
  imports: [TypeOrmModule.forFeature([User, Statistic, Equipment])],
  providers: [UserService, UserGateway, StatisticService, EquipmentService],
})
export class UserModule {}
