import { User } from '../user.entity';
import { Omit } from '../../../types/common';
import { Equipment } from 'src/modules/equipment/equipment.entity';
import { Statistic } from 'src/modules/statistic/statistic.entity';

export type GenderType = 'male' | 'female';

export type IUpdateUserBaseInfo = Partial<User> & Omit<User, 'statistic'>;

export interface ICreateUserInfo {
  first_name: string;
  last_name: string;
  nickname: string;
  gender: GenderType;
  statistic: Statistic;
  equipment: Equipment[];
}
