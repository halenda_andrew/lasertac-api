import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { RepositoryType } from '../../consts';

@Entity(RepositoryType.RANKED)
export class Ranked {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'varchar', length: 256 })
  name: string;
}
