import {
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
} from '@nestjs/websockets';
import { Server, Client } from 'socket.io';

import { StatisticService } from './statistic.service';
import { ErrorType } from '../../consts';
import { IUpdateStatisticInfo } from './interfaces';

@WebSocketGateway()
export class StatisticGateway {
  @WebSocketServer()
  server: Server;

  constructor(private readonly statisticService: StatisticService) {}

  @SubscribeMessage('updateUserStats')
  async updateUserStats(_client: Client, payload: IUpdateStatisticInfo) {
    try {
      await this.statisticService.updateOne(payload);

      return {
        event: 'userStatsUpdated',
        data: '',
      };
    } catch (err) {
      console.log(err);
      return {
        event: 'serverError',
        data: ErrorType.updateUserStatsError,
      };
    }
  }
}
