import { Statistic } from '../statistic.entity';

export type IUpdateStatisticInfo = Partial<Statistic>;
