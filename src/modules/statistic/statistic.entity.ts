import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';
import { RepositoryType } from '../../consts';

@Entity(RepositoryType.STATISTIC)
export class Statistic {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ type: 'int', default: 0 })
  steps: number;

  @Column({ type: 'int', default: 0 })
  total_battles: number;

  @Column({ type: 'int', default: 0 })
  total_ammo: number;

  @Column({ type: 'int', default: 0 })
  total_kills: number;

  @Column({ type: 'int', default: 0 })
  life_lost: number;

  @Column({ type: 'int', default: 0 })
  score_points: number;

  @Column({ type: 'int', default: 0 })
  time_spent: number;

  @Column({ type: 'int', default: 0 })
  likes: number;

  @Column({ type: 'double', default: 0 })
  kda: number;
}
