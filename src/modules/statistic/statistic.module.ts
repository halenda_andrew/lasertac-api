import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Statistic } from './statistic.entity';
import { StatisticService } from './statistic.service';
import { StatisticGateway } from './statistic.gateway';

@Module({
  imports: [TypeOrmModule.forFeature([Statistic])],
  providers: [StatisticService, StatisticGateway],
})
export class StatisticModule {}
