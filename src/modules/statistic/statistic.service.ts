import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Statistic } from './statistic.entity';
import { IUpdateStatisticInfo } from './interfaces';

@Injectable()
export class StatisticService {
  constructor(
    @InjectRepository(Statistic)
    private readonly statisticRepository: Repository<Statistic>,
  ) {}

  async addOne() {
    return await this.statisticRepository.save({});
  }

  async updateOne({ id, ...rest }: IUpdateStatisticInfo) {
    return this.statisticRepository.update({ id }, rest);
  }
}
