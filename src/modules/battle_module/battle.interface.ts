export interface BattleInterface {
    id?: number;
    battle_name: string;
    battle_creator: number;
    battle_meeting_date: number;
    battle_meting_time: number;
    battle_coordinates: string;
    searching_radius: number;
    safe_zone: number;
    players_count: number;
    max_count_players: number;
    players_lives: number;
    bleedout_time: number;
    ammo_magazines: number;
    team1_id: number;
    team2_id: number;  
}