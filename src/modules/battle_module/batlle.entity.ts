import { Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne } from "typeorm";
import { BattleTeamsTable } from "../teams_module/battle_teams.entity";
import { User } from "src/modules/user/user.entity";

@Entity('battle_game')
export class BattleTable {

    @PrimaryGeneratedColumn()
    id: number;
    
    @Column()
    battle_name: string;

    @ManyToOne(type => User, user => user.created_battles)
    battle_creator: number;

    @Column()
    battle_meeting_date: number;

    @Column()
    battle_meeting_time: number;

    @Column()
    battle_coordinates: string;
    
    @Column()
    battle_duration: number;

    @Column()
    searching_radius: number;

    @Column()
    safe_zone: number;

    @Column()
    players_count: number;

    @Column()
    max_count_players: number;

    @OneToMany(type => BattleTeamsTable, BattleTeamsTable => BattleTeamsTable.battle)
    team1: BattleTeamsTable[];

    @OneToMany(type => BattleTeamsTable, BattleTeamsTable => BattleTeamsTable.battle)
    team2: BattleTeamsTable[];

    @Column()
    players_lives: number;

    @Column()
    bleedout_time: number;

    @Column()
    ammo_magazine: number;


}