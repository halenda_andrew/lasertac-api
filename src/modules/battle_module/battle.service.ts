import { Injectable } from "@nestjs/common";
import { BattleTable } from "./batlle.entity";
import { BattleInterface } from "./battle.interface";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";

@Injectable()
export class BattleService {

    constructor(
        @InjectRepository(BattleTable)
        private readonly battle: Repository<BattleTable>,
    ) {}

    async createBattle(params: BattleInterface) {
        return await this.battle.create(params);
    }

    async getBattles(): Promise<BattleTable[]> {
        return await this.battle.find();
    }

    async getBattle(id: number): Promise<BattleTable> {
        return await this.battle.findOne(id);
    }

    async deleteBattle(id: number) {
        return await this.battle.delete(id);
    }

}